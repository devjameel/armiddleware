// will contain all of my user related routes
const express = require('express')
const mysql = require('mysql')
const router = express.Router()
const cv = require('opencv4nodejs');



const detectAndComputeAsync = (det, img) =>
  det.detectAsync(img)
    .then(kps => det.computeAsync(img, kps)
                      .then(desc => ({ kps, desc }))
    );

const img1 = cv.imread('data/per.jpg');
const img2 = cv.imread('data/husky.jpg');

const detectorNames = [
  // 'AKAZE',
  // 'BRISK',
  // 'KAZE',
  'ORB'
];


const createDetectorFromName = name => new cv[`${name}Detector`]();


router.get('/messages', (req, res) => {
  console.log("11111111")
  res.end()
})

router.get("/api/users", (req, res) => {
    const connection = getConnection()
    const queryString = "SELECT * FROM users"
    connection.query(queryString, (err, rows, fields) => {
      if (err) {
        console.log("Failed to query for users: " + err)
        res.sendStatus(500)
        return
      }
      res.json(rows)
    })
  })

  router.get("/api/tdmodels", (req, res) => {
    const connection = getConnection()
    const queryString = "SELECT * FROM tdmodel"
    connection.query(queryString, (err, rows, fields) => {
      if (err) {
        console.log("Failed to query for users: " + err)
        res.sendStatus(500)
        return
      }
      res.json(rows)
    })
  })

const pool = mysql.createPool({
    connectionLimit: 10,
    host: '95.216.242.100',
    user: 'aradmin',
    password:'25s^nHg4',
    database: 'ardb'
})

function getConnection() {
    return pool
}


router.post('/match', function(req, res) {
  const promises = detectorNames
  .map(createDetectorFromName)
  .map(det =>
    // also detect and compute descriptors for img1 and img2 async
    Promise.all([detectAndComputeAsync(det, img1), detectAndComputeAsync(det, img2)])
      .then(allResults =>
        cv.matchBruteForceAsync(
          allResults[0].desc,
          allResults[1].desc
        )
        .then(matches => ({
          matches,
          kps1: allResults[0].kps,
          kps2: allResults[1].kps
        }))
        .then(function(value) {


          
          console.log(value.matches);
          // expected output: "foo"
        }
      )
      )

);


});

router.post('/imageRating', function(req, res) {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded.');
  }

  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let image = req.files.image;

  // Use the mv() method to place the file somewhere on your server
  image.mv('uploadedImages/filename.jpg', function(err) {
    if (err)
      return res.status(500).send(err);
      const mat = cv.imread('uploadedImages/filename.jpg');

    res.send('File uploaded!');
  });




});

router.post('/api/tdmodels/uploadTDModel', function(req, res) {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded.');
  }

  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let image = req.files.image;
  let name = image.name;

  // Use the mv() method to place the file somewhere on your server
  image.mv('uploadedTDModel/'+name, function(err) {
    if (err)
      return res.status(500).send(err);

      const queryString = "INSERT INTO tdmodel (url,name) VALUES (?,?)" 
      getConnection().query(queryString, ['uploadedTDModel/'+name,name], (err, results, fields) => {
        if (err) {
          console.log("Failed to insert new user: " + err)
          res.sendStatus(500)
          return
        }
    
        console.log("Inserted a new tdmodel with id: ", results.insertId);
        res.end()
      })

    res.send('File uploaded!');
  });




});




router.post('/tdmodel_delete', (req, res) => {
    // your JSON

  const id = req.body.id

  // const lastName = req.body.create_last_name

  const queryString = "Delete from tdmodel where id = ?"
  getConnection().query(queryString, [id], (err, results, fields) => {
    if (err) {
      console.log("Failed to insert new user: " + err)
      res.sendStatus(500)
      return
    }

    console.log("Inserted a new user with id: ", results.insertId);
    res.end()
  })
})


router.post('/api/users/createUser', (req, res) => {
    console.log("Trying to create a new user...")
    console.log("How do we get the form data???")
    console.log(req.body);      // your JSON

    console.log("First name: " + req.body.userName)
    const userName = req.body.userName
    const password = req.body.password

    // const lastName = req.body.create_last_name
  
    const queryString = "INSERT INTO users (UserName,Password) VALUES (?,?)"
    getConnection().query(queryString, [userName,password], (err, results, fields) => {
      if (err) {
        console.log("Failed to insert new user: " + err)
        res.sendStatus(500)
        return
      }
  
      console.log("Inserted a new user with id: ", results.insertId);
      res.end()
    })
  })
  
router.get('/api/users/:id', (req, res) => {
    console.log("Fetching user with id: " + req.params.id)

    const connection = getConnection()

    const userId = req.params.id
    const queryString = "SELECT * FROM users WHERE UserID = ?"
    connection.query(queryString, [userId], (err, rows, fields) => {
        if (err) {
        console.log("Failed to query for users: " + err)
        res.sendStatus(500)
        return
        // throw err
        }

        console.log("I think we fetched users successfully")

        const users = rows.map((row) => {
        return {UserID: row.UserID, UserName: row.UserName}
        })

        res.json(users)
    })
})

router.post('/api/users/login', (req, res) => {
  console.log("Fetching user with id: " + req.params.userName)
  console.log("Fetching user with password: " + req.params.password)

  const connection = getConnection()

  const userName = req.body.userName
  const password = req.body.password

  const queryString = "SELECT * FROM users WHERE UserName = ? and Password = ?"
  connection.query(queryString, [userName,password], (err, rows, fields) => {
      if (err) {
      console.log("Failed to query for users: " + err)
      res.sendStatus(500)
      return
      // throw err
      }

      console.log("I think we fetched users successfully")

      const users = rows.map((row) => {
      return {UserID: row.UserID, UserName: row.UserName}
      })

      res.json(users)
  })
})


module.exports = router