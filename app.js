// load our app server using express somehow....
const express = require('express')
const fileUpload = require('express-fileupload');


const app = express()
const morgan = require('morgan')
const mysql = require('mysql')
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const http = require('http');
let port = process.env.PORT;


const bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({extended: false}))

app.use(express.static('./public'))
app.use(fileUpload());

app.use(morgan('short'))
app.use(express.json());

const router = require('./routes/user.js')

app.use(router)

app.get("/", (req, res) => {
  console.log("Responding to root route")
  res.send("Hello from ROOOOOT")
})

// localhost:443
http.createServer(app).listen(port, () => {
  console.log("Server is up and listening on ..."+port)
  app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));     


});

// app.listen(8080, () => {
//   console.log("Server is up and listening on 8080...")
//   app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));     


// })
